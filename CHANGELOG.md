## v2.0.1
- 发布2.0.1正式版，在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)上验证通过

## v2.0.0
- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法适配

### 1.0.3
1. 适配API 9 stage模型，DevEco 3.1.0.100。

### 1.0.2 
1. 适配hvigorfile编译系统。


### 1.0.0
1.OpenHarmony适配。
2.增加了REST API实现。